import express from 'express'
import {userRouter} from "./src/Delivery/Routes/User"
import {followRouter} from "./src/Delivery/Routes/Follow"
import {tweetRouter} from "./src/Delivery/Routes/Tweet"
import cors from 'cors';
const app = express();
const allowedOrigins = ['http://localhost:3000'];

const options: cors.CorsOptions = {
  origin: allowedOrigins
};

app.use(express.json())
app.use(cors(options))
app.use('/users', userRouter);
app.use('/follow', followRouter);
app.use('/tweets', tweetRouter);
app.listen(3333, () => {
  console.log('Server started');
});
