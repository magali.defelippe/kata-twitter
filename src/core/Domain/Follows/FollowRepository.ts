import { Follow } from "./Follow";

export interface FollowRepository {
    create(newFollowing: Follow): Promise<void>;
    getFollowers(nickname: string): Promise<string[]>;
    getFollowings(nickname: string): Promise<string[]>;
}