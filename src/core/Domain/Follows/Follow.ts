export interface Follow {
    follower: string;
    following: string;
}