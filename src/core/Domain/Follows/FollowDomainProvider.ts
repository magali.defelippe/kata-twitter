import { SequelizeFollowRepository } from "../../Infraestructure/Follows/SequelizeFollowRepository";
import { FollowRepository } from "./FollowRepository";
const {sequelize} = require('../../../../sequelize/models/');

export function followRepositoryProvider(): FollowRepository{
    const model = sequelize.models.Follow;
    return new SequelizeFollowRepository(model);
};