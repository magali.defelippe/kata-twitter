import { SequelizeUserRepository } from "../../Infraestructure/User/SequelizeUserRepository";
import { UserRepository } from "./UserRepository";
const {sequelize} = require('../../../../sequelize/models/');

export function userRepositoryProvider(): UserRepository{
    const model = sequelize.models.User;
    return new SequelizeUserRepository(model);
}