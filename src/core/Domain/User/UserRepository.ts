import { User } from "./User"

export interface UserRepository {
    save(user: User, nickname: string): Promise<void>;
    exist(nickname: string): Promise<boolean>;
    find(nickname: string): Promise<User | undefined>;
    updateName(nickname: string, newName: string): Promise<void>;
}