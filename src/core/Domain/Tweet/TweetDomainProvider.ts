import { SequelizeTweetRepository } from "../../Infraestructure/Tweet/SequelizeTweetRepository";
import { TweetRepository } from "./TweetRepository";
const {sequelize} = require('../../../../sequelize/models/');

export function tweetRepositoryProvider(): TweetRepository{
    const model = sequelize.models.Tweet;
    return new SequelizeTweetRepository(model);
};