import { Tweet } from "./Tweet"

export interface TweetRepository {
    create(tweet: Tweet): Promise<void>;
    get(nickname: string): Promise<string[]>
}