export interface Tweet{
    content: string,
    author: string
}