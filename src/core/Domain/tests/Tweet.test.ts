import {Tweet} from '../Tweet/Tweet';

describe("Tweet case use", () => {
    test("Create a new tweet", () => {
        const content = 'Esto es un tweet';
        const author = '@maga'; 
        
        const tweet: Tweet = {
            content,
            author,
        };
        
        expect(tweet.content === content);
        expect(tweet.author === author);

    })
})