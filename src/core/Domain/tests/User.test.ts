import { User } from '../User/User';

describe("User case use", () => {
    test("Create a new user", () => {
        const name = 'maga';
        const nickname = '@maga'
        const user: User = {
            name,
            nickname
        };

        expect(user.name === name);
    })
})