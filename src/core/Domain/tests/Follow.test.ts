import { Follow } from '../Follows/Follow';

describe("Follows case use", () => {
    test("Create a new follow person", () => {
        const following = '@pato';
        const follower = '@maga'; 
        
        const follow: Follow = {
            following,
            follower,
        };
        
        expect(follow.follower === follower);
        expect(follow.following === following);

    })
})