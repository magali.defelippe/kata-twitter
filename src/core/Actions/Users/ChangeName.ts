import { UserRepository } from '../../Domain/User/UserRepository';

export class ChangeName {

    constructor(private repository: UserRepository) { }

    public async execute(nickname: string, newName: string) {
       await this.repository.updateName(nickname, newName);
    }
}