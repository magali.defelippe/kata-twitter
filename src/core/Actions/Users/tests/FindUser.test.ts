import { FindUser } from '../FindUser';
import { UserRepository } from '../../../Domain/User/UserRepository';

describe("Find user use case", () => {
    const nickname = '@ironman';
    const repository = {} as UserRepository;

    beforeEach(() => {
        jest.clearAllMocks();
    })

    test("Can find a user only with his nickname", async () => {
        const user = {
            nickname,
            name: 'ironman'
        }
        repository.find = jest.fn().mockReturnValue(user)
        const findUser = new FindUser(repository);

        const userReturned = await findUser.execute(nickname);
        expect(repository.find).toBeCalledWith(nickname);
        expect(userReturned).toEqual(user)
    });

    test("Cant find a user because not exist", () => {
        repository.find = jest.fn()
        const findUser = new FindUser(repository);

        const message = findUser.execute(nickname);
        expect(() => message).rejects.toThrow();
    });

});
