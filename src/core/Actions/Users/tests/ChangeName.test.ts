import { ChangeName } from '../ChangeName';
import { UserRepository } from '../../../Domain/User/UserRepository';

describe("Change name user case", () => {

    test("An user can change their real name", () => {
        const repository = {} as UserRepository;
        repository.updateName = jest.fn()
        const changeName = new ChangeName(repository);
        const nickname = '@ironman';
        const newName = 'hola';

        changeName.execute(nickname, newName);
        expect(repository.updateName).toBeCalledWith(nickname, newName);
    });


});
