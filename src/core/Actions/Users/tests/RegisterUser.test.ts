import { RegisterUser } from '../RegisterUser';
import { UserRepository } from '../../../../core/Domain/User/UserRepository';

describe("Register user case", () => {
    const userRepository = {} as UserRepository;
    const registerUser = new RegisterUser(userRepository);
    userRepository.exist = jest.fn();
    userRepository.save = jest.fn();

    const name = 'nombre';
    const nickname = '@nickname';
    
    beforeEach(() => {
        jest.clearAllMocks();
    })

    test("If the nickname not exist add a new user", async () => {
        userRepository.exist = jest.fn().mockReturnValue(false);

        const message = await registerUser.execute(name, nickname);

        expect(() => message).not.toThrow()
    });

    test("If nickname exist throw an error", async () => {
        userRepository.exist = jest.fn().mockReturnValue(true);
        
        const message = registerUser.execute(name, nickname);
        expect(() => message).rejects.toThrow()
    });

    test("If the nickname not start with @ throw an error", async () => {
        const nicknameError = 'nickname';

        const validation = registerUser.execute('hola', nicknameError);
        expect(() => validation).rejects.toThrow()
    })

});
