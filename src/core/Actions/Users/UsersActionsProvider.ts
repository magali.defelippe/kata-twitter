import { userRepositoryProvider } from "../../Domain/User/UserDomainProvider";
import { ChangeName } from "./ChangeName";
import { FindUser } from "./FindUser";
import { RegisterUser } from "./RegisterUser";

const repository = userRepositoryProvider();

export function registerUserProvider(): RegisterUser{
    return new RegisterUser(repository);
};

export function findUserProvider(): FindUser{
    return new FindUser(repository);
};

export function changeNameProvider(): ChangeName{
    return new ChangeName(repository);
};