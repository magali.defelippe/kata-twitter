import { User } from '../../Domain/User/User';
import { UserRepository } from '../../Domain/User/UserRepository';

export class RegisterUser {

    constructor(private repository: UserRepository) { }

    public async execute(name: string, nickname: string) {
        const exist = await this.exist(nickname)
        if (!exist) {
            const user: User = {
                name,
                nickname
            };
            this.validateStartOfNickname(nickname);
            await this.save(user, nickname);
        } else {
            throw new Error("El nickname ya está en uso")
        }
    }

    private async save(user: User, nickname: string) {
        await this.repository.save(user, nickname)
    }

    private async exist(nickname: string) {
        return await this.repository.exist(nickname)
    }

    private validateStartOfNickname(nickname: string) {
        const validation = nickname.startsWith('@', 0);
        if (!validation) throw new Error("The nickname not starts with @");
    }
}
