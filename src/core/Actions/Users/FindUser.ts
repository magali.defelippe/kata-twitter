import { UserRepository } from '../../Domain/User/UserRepository';

export class FindUser {

    constructor(private repository: UserRepository) { }

    public async execute(nickname: string) {
        const user = await this.repository.find(nickname);
        if (user) {
            return user;
        }
        throw new Error("Usuario no encontrado");
    }
}