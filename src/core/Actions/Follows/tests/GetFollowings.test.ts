import { GetFollowings } from '../GetFollowings';
import { FollowRepository } from '../../../Domain/Follows/FollowRepository';

describe("GetFollowings of an user use case", () => {

    test("Execute the repository and get the followings of an user", () => {
        const repository = {} as FollowRepository;
        repository.getFollowings = jest.fn()

        const getFollowings = new GetFollowings(repository);
        const userNickname = '@maga';

        getFollowings.execute('@maga');
        expect(repository.getFollowings).toBeCalledWith(userNickname);
    });

});
