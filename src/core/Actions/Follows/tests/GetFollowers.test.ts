import { GetFollowers } from '../GetFollowers';
import { FollowRepository } from '../../../Domain/Follows/FollowRepository';

describe("GetFollowers of an user use case", () => {

    test("Execute the repository and get the followers of an user", () => {
        const repository = {} as FollowRepository;
        repository.getFollowers = jest.fn()

        const getFollowers = new GetFollowers(repository);
        const userNickname = '@maga';

        getFollowers.execute('@maga');
        expect(repository.getFollowers).toBeCalledWith(userNickname);
    });

});
