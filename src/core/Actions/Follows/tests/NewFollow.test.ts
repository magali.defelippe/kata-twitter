import { NewFollow } from '../NewFollow';
import { FollowRepository } from '../../../Domain/Follows/FollowRepository';
import { Follow } from '../../../Domain/Follows/Follow';

describe("NewFollow a user use case", () => {

    test("create a follow", () => {
        const repository = {} as FollowRepository;
        repository.create = jest.fn()

        const following = '@pato';
        const follower = '@maga';
        const newFollow = new NewFollow(repository);
        const follow: Follow = {
            following,
            follower,
        };
        newFollow.execute(following, follower);
        expect(repository.create).toBeCalledWith(follow);

    });

});
