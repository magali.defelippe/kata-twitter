import { FollowRepository } from "../../Domain/Follows/FollowRepository";

export class GetFollowers {
    constructor(private repository: FollowRepository) { }

    public async execute(user: string): Promise<string[]> {
        return await this.repository.getFollowers(user)
    }
}