import { FollowRepository } from "../../Domain/Follows/FollowRepository";

export class GetFollowings {
    constructor(private repository: FollowRepository) { }

    public async execute(user: string): Promise<string[]> {
        return await this.repository.getFollowings(user)
    }
}