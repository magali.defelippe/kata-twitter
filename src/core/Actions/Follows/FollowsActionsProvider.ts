import { followRepositoryProvider } from "../../Domain/Follows/FollowDomainProvider";
import { NewFollow } from "./NewFollow";
import { GetFollowers } from "./GetFollowers";
import { GetFollowings } from "./GetFollowings";

const repository = followRepositoryProvider();

export function newFollowProvider(): NewFollow{
    return new NewFollow(repository);
};

export function getFollowersProvider(): GetFollowers{
    return new GetFollowers(repository);
};

export function getFollowingsProvider(): GetFollowings{
    return new GetFollowings(repository);
};