import { Follow } from "../../Domain/Follows/Follow";
import { FollowRepository } from "../../Domain/Follows/FollowRepository";

export class NewFollow {
    constructor(private repository: FollowRepository) { }

    public async execute(follower: string, following: string) {
        const newFollow: Follow = {
            follower,
            following
        };
        await this.repository.create(newFollow)
    }
}