import { TweetRepository } from "../../Domain/Tweet/TweetRepository";

export class GetTweets{
    constructor(private repository: TweetRepository){}

    public async execute(nickname: string): Promise<string[]>{
        return await this.repository.get(nickname);
    }
}