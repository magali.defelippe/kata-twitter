import {GetTweets} from '../GetTweets';
import { TweetRepository } from '../../../Domain/Tweet/TweetRepository';

describe("Get tweets use case", () => {
    const tweetRepository = {} as TweetRepository;
    const getTweets = new GetTweets(tweetRepository);
    const tweetsOfTheUser = [{content: 'chau'}, {content: 'hola'}];
    tweetRepository.get = jest.fn().mockReturnValue(tweetsOfTheUser);

    test("Get the tweets of an user", async () => {
        const author = '@maga'

        const tweets = await getTweets.execute(author);
        
        expect(tweetRepository.get).toBeCalledWith(author);
        expect(tweets).toEqual(tweetsOfTheUser);
    })
})