import {CreateTweet} from '../CreateTweet';
import { TweetRepository } from '../../../Domain/Tweet/TweetRepository';
import { Tweet } from '../../../Domain/Tweet/Tweet';

describe("Create tweet use case", () => {
    const tweetRepository = {} as TweetRepository;
    const createTweet = new CreateTweet(tweetRepository);
    tweetRepository.create = jest.fn();

    test("Create a tweet", async () => {
        const content = 'Tweet de prueba';
        const author = '@maga'
        const tweet: Tweet = {
            content,
            author
        };
      

        await createTweet.execute(content, author);
        
        expect(tweetRepository.create).toBeCalledWith(tweet);
    })
})