import { Tweet } from '../../Domain/Tweet/Tweet';
import { TweetRepository } from '../../Domain/Tweet/TweetRepository';
export class CreateTweet{
    constructor(private repository: TweetRepository){}

    public async execute(content: string, author: string){
        this.validate(content);
        const tweet: Tweet = {
            content,
            author
        }
        this.repository.create(tweet);
    }

    private validate(content: string){
        if(content.length < 2) throw new Error('Tweet muy corto');
    }
}