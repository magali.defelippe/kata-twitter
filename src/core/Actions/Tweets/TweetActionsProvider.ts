import { tweetRepositoryProvider } from "../../Domain/Tweet/TweetDomainProvider";
import { CreateTweet } from "./CreateTweet";
import { GetTweets } from "./GetTweets";

const repository = tweetRepositoryProvider();

export function createTweetProvider(): CreateTweet{
    return new CreateTweet(repository);
};

export function getTweetsProvider(): GetTweets{
    return new GetTweets(repository);
};