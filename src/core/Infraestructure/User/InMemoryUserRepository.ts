import { User } from "../../Domain/User/User";
import { UserRepository } from "../../Domain/User/UserRepository";

interface InMemoryUser {
    name: string;
}

export class InMemoryUserRepository implements UserRepository {
    private users: Map<string, InMemoryUser> = new Map<string, InMemoryUser>();

    public async find(nickname: string): Promise<User | undefined> {
        const user = await this.users.get(nickname);
        if (user) return { name: user.name, nickname };
    }

    public async save(user: User, nickname:string): Promise<void> {
        await this.users.set(nickname, user)
    }

    public async exist(nickname: string): Promise<boolean> {
        const searchNickname = await this.find(nickname);
        return searchNickname !== undefined;
    }

    public async updateName(nickname: string, newName: string): Promise<void> {
        const userToUpdateName = await this.find(nickname);
        if (userToUpdateName !== undefined) userToUpdateName.name = newName;
    }
}