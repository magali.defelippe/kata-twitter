import { UserRepository } from "../../Domain/User/UserRepository";
import { User } from "../../Domain/User/User";


export class SequelizeUserRepository implements UserRepository {
    constructor(private model: any){}

    public async save(user: User, nickname: string): Promise<void>{      
       await this.model.create({ nickname: nickname, name: user.name})  
    }

    public async exist(nickname: string):  Promise<boolean>{
        const checkExist = await this.find(nickname);
        return checkExist !== undefined;
    } 

    public async find(nickname: string): Promise<User | undefined>{
        const user = await this.model.findOne({ where: { nickname } })
        if(user) return { name: user.name, nickname: user.nickname}
    }
    
    public async updateName(nickname: string, newName: string): Promise<void>{
        const user = await this.find(nickname);
        if(user){ await this.model.update({nickname, name: newName}, {where: { nickname }})}
        else{
            throw new Error("Usuario no encontrado");
        };
    }
}
