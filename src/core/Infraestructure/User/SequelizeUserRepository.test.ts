import {Sequelize} from 'sequelize';
import {SequelizeUserRepository} from './SequelizeUserRepository';
import { User } from "../../Domain/User/User";
import { when } from 'jest-when'

describe("Sequelize user repository use cases", () => {
    
    const {sequelize, findOne } = setupSequelize();
    const userModel = sequelize.models.User;
    const repository = new SequelizeUserRepository(userModel)
    const name = 'hola';
    const nickname = '@hola';
    const user: User = {
      name,
      nickname
    };

    beforeEach(() => {
      jest.clearAllMocks();
    })

    test("Can save a new user", async () => {
        await repository.save(user, nickname);
        expect(userModel.create).toBeCalledWith(user);
    })

    test("Can find a user", async () => {
      const findParameter = { where: {nickname: nickname}};

      when(findOne).calledWith(findParameter).mockReturnValue(user);
      const userFind = await repository.find(nickname);
      expect(userFind).toEqual(user);
      expect(findOne).toBeCalled();
  })

  test("Can return if the user exist or not", async () => {
    repository.find = jest.fn();

    const retornedValue = await repository.exist(nickname);
   
    expect(repository.find).toBeCalled();
    expect(retornedValue).toBeFalsy();
  })

  test("Can update the name of an user", async () => {
    const newName = 'chau';
    repository.find = jest.fn().mockReturnValue(user);
    await repository.updateName(nickname, newName);
    expect(userModel.update).toBeCalledWith({nickname, name: newName}, {where: { nickname }});
  })
})

function setupSequelize(): {
    sequelize: Sequelize;
    create: jest.Mock;
    findOne: jest.Mock;
    update: jest.Mock;
  } {
    const create = jest.fn();
    const findOne = jest.fn();
    const update = jest.fn();
    const sequelize = {
      models: { User: { create, findOne, update } },
    } as unknown;
    return { sequelize: sequelize as Sequelize, create, findOne, update };
}