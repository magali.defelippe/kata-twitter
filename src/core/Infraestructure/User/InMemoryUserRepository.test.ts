import { User } from '../../Domain/User/User';
import { InMemoryUserRepository } from './InMemoryUserRepository'

describe('InMemoryUserRepository case use', () => {
    const inMemoryUserRepository = new InMemoryUserRepository();
    const nickname = '@maga';
    const name = 'maga';

    const user: User = {
        name,
        nickname
    };

    test('Can save a user', async() => {
        inMemoryUserRepository.save(user, nickname);
        const userFind = await inMemoryUserRepository.find(nickname);
        expect(userFind).toEqual(user);
    });

    test('if the nickname exist, return true', async () => {
        const result = await inMemoryUserRepository.exist(nickname);
        expect(result).toBeTruthy();
    });

    test('if the nickname not exist, return false', async () => {
        const nickname = '@lala';
        const result = await inMemoryUserRepository.exist(nickname);
        expect(result).toBeFalsy();
    });

})