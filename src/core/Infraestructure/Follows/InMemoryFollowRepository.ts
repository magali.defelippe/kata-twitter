import { Follow } from "../../Domain/Follows/Follow";
import { FollowRepository } from "../../Domain/Follows/FollowRepository";

interface InMemoryFollow {
    followers: string[];
    following: string[];
}

export class InMemoryFollowRepository implements FollowRepository {
    private followings: Map<string, InMemoryFollow> = new Map<string, InMemoryFollow>();

    public async create(newFollowing: Follow): Promise<void> {
        const newFollow = newFollowing;
        await this.addFollowing(newFollow);
        await this.addFollower(newFollow);
    }

    private async addFollowing(newFollowing: Follow) {
        const user = await this.followings.get(newFollowing.follower);
        if (user !== undefined) {
            user.following.push(newFollowing.following)
        } else {
            const follow: InMemoryFollow = {
                following: [newFollowing.following],
                followers: [],
            }
            await this.followings.set(newFollowing.follower, follow);
        }
    }

    private async addFollower(newFollowing: Follow) {
        const user = await this.followings.get(newFollowing.following);
        if (user !== undefined) {
            user.followers.push(newFollowing.follower)
        } else {
            const follow: InMemoryFollow = {
                following: [],
                followers: [newFollowing.follower],
            }
            await this.followings.set(newFollowing.following, follow);
        }
    }

    public async getFollowers(nickname: string): Promise<string[]> {
        const user = await this.followings.get(nickname);
        if (user !== undefined) {
            return user.followers;
        }
        else {
            throw new Error("Usuario no encontrado");
        }
    }

    public async getFollowings(nickname: string): Promise<string[]> {
        const user = await this.followings.get(nickname);
        if (user !== undefined) {
            return user.following;
        }
        else {
            throw new Error("Usuario no encontrado");
        }
    }
}