import { Follow } from "../../Domain/Follows/Follow";
import { FollowRepository } from "../../Domain/Follows/FollowRepository";

export class SequelizeFollowRepository implements FollowRepository {
   
    constructor(private model: any){}

    public async create(newFollow: Follow): Promise<void> {
        await this.model.create({follower: newFollow.follower, following: newFollow.following});
    }

    public async getFollowers(nickname: string): Promise<string[]> {
        const followers = await this.model.findAll({where: {following: nickname}})
        if(followers){
           const followersOfUser = followers.map((follow: Follow) => follow.follower);
           return followersOfUser;
        } 
        return [];
    }

    public async getFollowings(nickname: string): Promise<string[]> {
        const followings = await this.model.findAll({where: {follower: nickname}})
        if(followings){
           const followingsOfUser = followings.map((follow: Follow) => follow.following);
           return followingsOfUser;
        }
        return [];
    }
}