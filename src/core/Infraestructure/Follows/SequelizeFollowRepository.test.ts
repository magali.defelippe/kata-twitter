import {Sequelize} from 'sequelize';
import {SequelizeFollowRepository} from './SequelizeFollowRepository';
import { Follow } from '../../Domain/Follows/Follow';
import {when} from 'jest-when';


describe("Sequelize user repository use cases", () => {
    
    const {sequelize, findAll} = setupSequelize();
    const model = sequelize.models.Follow;
    const repository = new SequelizeFollowRepository(model);

    const follower = '@chau';
    const following = '@hola';

    const follow: Follow = {
      follower,
      following
    };

    beforeEach(() => {
      jest.clearAllMocks();
    })

    test("Can create new follow", async () => {
        await repository.create(follow);
        expect(model.create).toBeCalledWith(follow);
    })

    test("Can get followers", async () => {
      const nickname = '@hola';
      const parameter = {where: {following: nickname}}
      when(findAll).calledWith(parameter).mockReturnValue([follow]);
      const followers = await repository.getFollowers(nickname);
      
      expect(model.findAll).toBeCalledWith(parameter);
      expect(followers).toEqual(['@chau'])
    })

    test("Can get followings", async () => {
      const nickname = '@hola';
      const parameterFollowing = {where: {follower: nickname}}

      when(findAll).calledWith(parameterFollowing).mockReturnValue([follow]);

      const followings = await repository.getFollowings(nickname);
      
      expect(model.findAll).toBeCalledWith(parameterFollowing);
      expect(followings).toEqual(['@hola'])
    })

    function setupSequelize(): {
      sequelize: Sequelize;
      create: jest.Mock;
      findOne: jest.Mock;
      update: jest.Mock;
      findAll: jest.Mock;
    } {
      const create = jest.fn();
      const findOne = jest.fn();
      const update = jest.fn();
      const findAll = jest.fn();
      const sequelize = {
        models: { Follow: { create, findOne, update, findAll } },
      } as unknown;
      return { sequelize: sequelize as Sequelize, create, findOne, update, findAll };
  }
})

