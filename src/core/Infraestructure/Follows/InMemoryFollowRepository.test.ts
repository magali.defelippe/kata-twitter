import { Follow } from "../../Domain/Follows/Follow"
import { InMemoryFollowRepository } from "./InMemoryFollowRepository"

describe('InMemoryFollowRepository case use', () => {
    const inMemoryFollowRepository = new InMemoryFollowRepository();

    const following = '@maga';
    const follower = '@deve';

    const newFollowing: Follow = {
        following,
        follower,
    };

    test("Can add following and follower", async () => {
        const deveInRepo = { followers: [], following: ['@maga'] };
        const magaInRepo = { followers: ['@deve'], following: [] };

        await inMemoryFollowRepository.create(newFollowing);

        const deveFollowers = await inMemoryFollowRepository.getFollowers('@deve');
        const magaFollowers = await inMemoryFollowRepository.getFollowers('@maga');

        expect(deveFollowers).toEqual(deveInRepo.followers);
        expect(magaFollowers).toEqual(magaInRepo.followers);
    })

    test('Can return the followers of a user only with his nickname', async () => {
        const followers = await inMemoryFollowRepository.getFollowers('@maga');

        expect(followers).toEqual(['@deve']);
    });

    test('Can return the followings of a user only with his nickname', async () => {
        const followers = await inMemoryFollowRepository.getFollowings('@deve');

        expect(followers).toEqual(['@maga']);
    });
})