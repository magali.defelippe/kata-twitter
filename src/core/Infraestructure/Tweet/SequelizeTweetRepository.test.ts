import {Sequelize} from 'sequelize';
import {SequelizeTweetRepository} from './SequelizeTweetRepository';
import {when} from 'jest-when';
import { Tweet } from '../../Domain/Tweet/Tweet';

describe("Sequelize tweet repository use case", () => {
       
    const {sequelize, findAll} = setupSequelize();
    const model = sequelize.models.Tweet;
    const repository = new SequelizeTweetRepository(model);

    const tweet: Tweet = {
        content: 'fake content',
        author: '@maga'
    }

    beforeEach(() => {
        jest.clearAllMocks();
    });
    
    test("Can save a new tweet", async () => {
        await repository.create(tweet);
        expect(model.create).toBeCalledWith(tweet);
    });

    test("Can return the tweets of an user", async () => {
        const nickname = '@maga';
        const parameter = {where: {author: nickname}}

        when(findAll).calledWith(parameter).mockReturnValue([tweet]);

        const tweets = await repository.get(nickname);

        expect(model.findAll).toBeCalledWith(parameter);
        expect(tweets).toEqual([tweet.content])
    });

    function setupSequelize(): {
        sequelize: Sequelize;
        create: jest.Mock;
        findOne: jest.Mock;
        update: jest.Mock;
        findAll: jest.Mock;
      } {
        const create = jest.fn();
        const findOne = jest.fn();
        const update = jest.fn();
        const findAll = jest.fn();
        const sequelize = {
          models: { Tweet: { create, findOne, update, findAll } },
        } as unknown;
        return { sequelize: sequelize as Sequelize, create, findOne, update, findAll };
    }
});