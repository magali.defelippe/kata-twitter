import { Tweet } from "../../Domain/Tweet/Tweet";
import { TweetRepository } from "../../Domain/Tweet/TweetRepository";

interface InMemoryTweet {
    content: string[];
};

export class InMemoryTweetRepository implements TweetRepository{
    private tweets: Map<string, InMemoryTweet> = new Map<string, InMemoryTweet>();

    public async create(tweet: Tweet): Promise<void> {
        const user = await this.tweets.get(tweet.author);
        if(user){
            user.content.push(tweet.content);
        }else{
            const newTweet: InMemoryTweet = {
                content: [tweet.content]
            }
            await this.tweets.set(tweet.author, newTweet);
        }
    };

    public async get(author: string): Promise<string[]>{
        const tweets = await this.tweets.get(author);
        if (tweets) return tweets.content;
        else {
            throw new Error("Usuario no encontrado");
        }
    }
}