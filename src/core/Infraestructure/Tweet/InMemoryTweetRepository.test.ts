import { Tweet } from '../../Domain/Tweet/Tweet';
import { InMemoryTweetRepository } from './InMemoryTweetRepository';

describe("InMemoryTweetRepository use cases", () => {
    const repository = new InMemoryTweetRepository();
    const content = 'Hola hola';
    const author = '@maga';

    const tweet: Tweet = {
        content, author
    };

    test("Can save a tweet", async () => {
        await repository.create(tweet);
        const findTweet = await repository.get(author);

        expect(findTweet).toEqual([tweet.content]);
    });

    test("Can get the tweets of an user", async () => {
       
        const findTweet = await repository.get(author);

        expect(findTweet).toEqual([tweet.content]);
    });
});