import { Tweet } from "../../Domain/Tweet/Tweet";
import { TweetRepository } from "../../Domain/Tweet/TweetRepository";

export class SequelizeTweetRepository implements TweetRepository{
    constructor(private model: any){};

    public async create(tweet: Tweet): Promise<void>{
        await this.model.create(tweet);
    }

    public async get(nickname: string): Promise<string[]> {
        const tweets = await this.model.findAll({where: {author: nickname}})
        if(tweets){
           const tweetsOfUser = tweets.map((tweet: Tweet) => tweet.content);
           return tweetsOfUser;
        } 
        return [];
    }
}