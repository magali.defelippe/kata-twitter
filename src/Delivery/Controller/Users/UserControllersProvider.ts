import { registerUserProvider, findUserProvider, changeNameProvider} from "../../../core/Actions/Users/UsersActionsProvider";
import { ChangeNameController } from "./ChangeNameController";
import { FindUserController } from "./FindUserController";
import { RegisterUserController } from "./RegisterUserController";


export function registerController(): RegisterUserController {
    const registerUser = registerUserProvider();
    return new RegisterUserController(registerUser);
}

export function findController(): FindUserController {
    const findUser = findUserProvider();
    return new FindUserController(findUser);
}

export function changeNameController(): ChangeNameController{
    const changeName = changeNameProvider();
    return new ChangeNameController(changeName);
}