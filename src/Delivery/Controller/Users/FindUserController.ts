import * as express from 'express';
import { FindUser } from '../../../core/Actions/Users/FindUser';


export class FindUserController {
    constructor(private action: FindUser){}
    
    async execute(req: express.Request, res: express.Response): Promise<string| any> {
        try {
            const response = await this.action.execute(req.params.nickname);
            return res.status(200).send(response)
        } catch (error) {
            res.status(500).send(error.message);
        }
    }
}