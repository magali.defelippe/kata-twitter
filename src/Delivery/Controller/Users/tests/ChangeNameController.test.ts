import { ChangeName } from '../../../../core/Actions/Users/ChangeName';
import { ChangeNameController } from '../ChangeNameController';
import * as express from 'express';


describe("RegisterUserController use case", () => {
    const changeName = {} as ChangeName;
    const changeNameController = new ChangeNameController(changeName);
    const request = { 
        body: {
            newName: 'hola'
        } 
    } as express.Request;
    request.params = { nickname: '@maga' };

    const response = {} as express.Response;
    response.status = jest.fn().mockReturnValue(response);
    response.send = jest.fn();

    beforeEach(() => {
        jest.clearAllMocks();
    })

    test("Can change the name of an user", async () => {
        changeName.execute = jest.fn();

        await changeNameController.execute(request, response);
        expect(changeName.execute).toBeCalledWith(request.params.nickname,request.body.newName);
        expect(response.status).toBeCalledWith(200);
        expect(response.send).toBeCalledWith('Nombre cambiado')
    })

    test("Cant register an user", async () => {
        changeName.execute = jest.fn().mockRejectedValue(() => {throw new Error('Usuario ya registrado')});

        await changeNameController.execute(request, response);
        expect(changeName.execute).toBeCalledWith(request.params.nickname,request.body.newName);
        expect(response.status).toBeCalledWith(500);
        expect(response.send).toBeCalled();
    })
});
