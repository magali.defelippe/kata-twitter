import { RegisterUser } from '../../../../core/Actions/Users/RegisterUser';
import { RegisterUserController } from '../RegisterUserController';
import * as express from 'express';


describe("RegisterUserController use case", () => {
    const registerUser = {} as RegisterUser;
    const registerUserController = new RegisterUserController(registerUser);
    const request = { 
        body: {
            name: 'maga', 
            nickname: '@maga'
        } 
    } as express.Request;

    const response = {} as express.Response;
    response.status = jest.fn().mockReturnValue(response);
    response.send = jest.fn();

    beforeEach(() => {
        jest.clearAllMocks();
    })

    test("Can register an user", async () => {
        registerUser.execute = jest.fn();

        await registerUserController.execute(request, response);
        expect(registerUser.execute).toBeCalledWith(request.body.name, request.body.nickname);
        expect(response.status).toBeCalledWith(200);
        expect(response.send).toBeCalledWith('Usuario registrado')
    })

    test("Cant register an user", async () => {
        registerUser.execute = jest.fn().mockRejectedValue(() => {throw new Error('Usuario ya registrado')});
        await registerUserController.execute(request, response);
        expect(registerUser.execute).toBeCalledWith(request.body.name, request.body.nickname);
        expect(response.status).toBeCalledWith(500);
        expect(response.send).toBeCalled();
    })
});
