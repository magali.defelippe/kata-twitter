import { FindUser } from '../../../../core/Actions/Users/FindUser';
import { FindUserController } from '../FindUserController';
import * as express from 'express';


describe("RegisterUserController use case", () => {
    const findUser = {} as FindUser;
    const findUserController = new FindUserController(findUser);

    const request = {} as express.Request;
    request.params = {nickname: '@hola'};

    const response = {} as express.Response;
    response.status = jest.fn().mockReturnValue(response);
    response.send = jest.fn();

    beforeEach(() => {
        jest.clearAllMocks();
    })

    test("Can find a user with his nickname", async () => {
        const responseUser = { name: 'hola', nickname: '@hola'}
        findUser.execute = jest.fn().mockReturnValue(responseUser);
       
        await findUserController.execute(request, response);
        expect(findUser.execute).toBeCalledWith(request.params.nickname);
        expect(response.status).toBeCalledWith(200);
        expect(response.send).toBeCalledWith(responseUser);
    })

    test("Cant find a user with his nickname", async () => {
        findUser.execute = jest.fn().mockRejectedValue(() => {throw new Error('Usuario no encontrado')});
       
        await findUserController.execute(request, response);
        expect(findUser.execute).toBeCalledWith(request.params.nickname);
        expect(response.status).toBeCalledWith(500);
        expect(response.send).toBeCalled();
    })
});
