import * as express from 'express';
import { ChangeName } from '../../../core/Actions/Users/ChangeName';


export class ChangeNameController {
    constructor(private action: ChangeName){}
    
    async execute(req: express.Request, res: express.Response): Promise<string| any> {
        try {
            await this.action.execute(req.params.nickname, req.body.newName);
            return res.status(200).send("Nombre cambiado");
        } catch (error) {
            res.status(500).send(error.message);
        }
    }
}