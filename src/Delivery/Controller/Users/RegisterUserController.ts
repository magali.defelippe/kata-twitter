import * as express from 'express';
import { RegisterUser } from '../../../core/Actions/Users/RegisterUser';


export class RegisterUserController {
    constructor(private action: RegisterUser){}
    
    async execute(req: express.Request, res: express.Response): Promise<string| any> {
        try {
            await this.action.execute(req.body.name, req.body.nickname);
            return res.status(200).send('Usuario registrado')
        } catch (error) {
            res.status(500).send(error.message);
        }
    }
}