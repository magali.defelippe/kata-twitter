import * as express from 'express';
import { GetFollowers } from '../../../core/Actions/Follows/GetFollowers';


export class GetFollowersController {
    constructor(private action: GetFollowers){}
    
    async execute(req: express.Request, res: express.Response): Promise<string| any> {
        try {
            const followers = await this.action.execute(req.params.nickname);
            return res.status(200).send(followers)
        } catch (error) {
            res.status(500).send(error.message);
        }
    }
}