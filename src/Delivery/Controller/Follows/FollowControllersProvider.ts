import { newFollowProvider, getFollowersProvider, getFollowingsProvider} from "../../../core/Actions/Follows/FollowsActionsProvider";
import { NewFollowController } from "./NewFollowController";
import { GetFollowersController } from "./GetFollowersController";
import { GetFollowingsController } from "./GetFollowingsController";


export function newFollowController(): NewFollowController {
    const newFollow = newFollowProvider();
    return new NewFollowController(newFollow);
}

export function getFollowersController(): GetFollowersController {
    const getFollowers = getFollowersProvider();
    return new GetFollowersController(getFollowers);
}

export function getFollowingsController(): GetFollowingsController {
    const getFollowings = getFollowingsProvider();
    return new GetFollowingsController(getFollowings);
}
