import * as express from 'express';
import { GetFollowings } from '../../../core/Actions/Follows/GetFollowings';


export class GetFollowingsController {
    constructor(private action: GetFollowings){}
    
    async execute(req: express.Request, res: express.Response): Promise<string| any> {
        try {
            const followings = await this.action.execute(req.params.nickname);
            return res.status(200).send(followings)
        } catch (error) {
            res.status(500).send(error.message);
        }
    }
}