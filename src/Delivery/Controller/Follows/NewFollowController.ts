import * as express from 'express';
import { NewFollow } from '../../../core/Actions/Follows/NewFollow';


export class NewFollowController {
    constructor(private action: NewFollow){}
    
    async execute(req: express.Request, res: express.Response): Promise<string| any> {
        try {
            await this.action.execute(req.body.follower, req.body.following);
            return res.status(200).send(`Has seguido a ${req.body.following} correctamente`)
        } catch (error) {
            res.status(500).send(error.message);
        }
    }
}