import { GetFollowings } from '../../../../core/Actions/Follows/GetFollowings';
import {GetFollowingsController} from '../GetFollowingsController';
import * as express from 'express';


describe("GetFollowersController use case", () => {
    const getFollowings = {} as GetFollowings;
    const getFollowingsController = new GetFollowingsController(getFollowings);

    const request = {} as express.Request;
    request.params = {nickname: '@maga'};

    const response = {} as express.Response;
    response.status = jest.fn().mockReturnValue(response);
    response.send = jest.fn();

    beforeEach(() => {
        jest.clearAllMocks();
    })

    test("Can return followings for an user", async () => {
        const mockFollowings = ['@deve', '@juanma']
        getFollowings.execute = jest.fn().mockReturnValue(mockFollowings);

        await getFollowingsController.execute(request, response);
        expect(getFollowings.execute).toBeCalledWith(request.params.nickname);
        expect(response.status).toBeCalledWith(200);
        expect(response.send).toBeCalledWith(mockFollowings);
    });

    test("Get an error when try to get followings", async () => {
        getFollowings.execute = jest.fn().mockRejectedValue(() => {throw new Error('Error')});

        await getFollowingsController.execute(request, response);
        expect(getFollowings.execute).toBeCalledWith(request.params.nickname);
        expect(response.status).toBeCalledWith(500);
        expect(response.send).toBeCalled();
    });
});
