import { NewFollow } from '../../../../core/Actions/Follows/NewFollow';
import {NewFollowController} from '../NewFollowController';
import * as express from 'express';


describe("NewFollowController use case", () => {
    const newFollow = {} as NewFollow;
    const newFollowController = new NewFollowController(newFollow);
    const request = { 
        body: {
            follower: '@maga', 
            following: '@deve'
        } 
    } as express.Request;

    const response = {} as express.Response;
    response.status = jest.fn().mockReturnValue(response);
    response.send = jest.fn();

    beforeEach(() => {
        jest.clearAllMocks();
    })

    test("create a follow", async () => {
    newFollow.execute = jest.fn();
      
        await newFollowController.execute(request, response);
        expect(newFollow.execute).toBeCalledWith(request.body.follower, request.body.following);
        expect(response.status).toBeCalledWith(200);
        expect(response.send).toBeCalled();
    });

    test("Get an error when try to create a follow", async () => {
        newFollow.execute = jest.fn().mockRejectedValue(() => {throw new Error('Error')});
  
        await newFollowController.execute(request, response);
        expect(response.status).toBeCalledWith(500);
        expect(newFollow.execute).toBeCalledWith(request.body.follower, request.body.following);
        expect(response.send).toBeCalled();
    });
});
