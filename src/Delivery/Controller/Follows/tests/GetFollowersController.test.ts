import { GetFollowers } from '../../../../core/Actions/Follows/GetFollowers';
import {GetFollowersController} from '../GetFollowersController';
import * as express from 'express';


describe("GetFollowersController use case", () => {
    const getFollowers = {} as GetFollowers;
    const getFollowersController = new GetFollowersController(getFollowers);
    const request = {} as express.Request;
    request.params = {nickname: '@maga'};

    const response = {} as express.Response;
    response.status = jest.fn().mockReturnValue(response);
    response.send = jest.fn()

    beforeEach(() => {
        jest.clearAllMocks();
    })

    test("Can return followers for an user", async () => {
        const mockFollowers = ['@deve', '@juanma']
        getFollowers.execute = jest.fn().mockReturnValue(mockFollowers);

        await getFollowersController.execute(request, response);
        expect(getFollowers.execute).toBeCalledWith(request.params.nickname);
        expect(response.status).toBeCalledWith(200);
        expect(response.send).toBeCalledWith(mockFollowers);
    });

    test("Get an error when try to get followers", async () => {
        getFollowers.execute = jest.fn().mockRejectedValue(() => {throw new Error('Error')});

        await getFollowersController.execute(request, response);
        expect(getFollowers.execute).toBeCalledWith(request.params.nickname);
        expect(response.status).toBeCalledWith(500);
        expect(response.send).toBeCalled();
    });
});
