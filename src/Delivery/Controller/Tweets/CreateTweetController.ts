import * as express from 'express';
import { CreateTweet } from '../../../core/Actions/Tweets/CreateTweet';


export class CreateTweetController {
    constructor(private action: CreateTweet){}
    
    async execute(req: express.Request, res: express.Response): Promise<string| any> {
        try {
            await this.action.execute(req.body.content, req.params.author);
            return res.status(200).send('Tweet creado');
        } catch (error) {
            res.status(500).send(error.message);
        }
    }
}