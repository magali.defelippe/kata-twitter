import { GetTweets } from '../../../../core/Actions/Tweets/GetTweets';
import { GetTweetsController } from '../GetTweetsController';
import * as express from 'express';


describe("RegisterUserController use case", () => {
    const getTweets = {} as GetTweets;
    const getTweetsController = new GetTweetsController(getTweets);

    const request = {} as express.Request;
    request.params = { author: '@maga' };

    const response = {} as express.Response;
    response.status = jest.fn().mockReturnValue(response);
    response.send = jest.fn();
    

    beforeEach(() => {
        jest.clearAllMocks();
    })

    test("Can get tweets of an user", async () => {
        const mockTweets = ['hola como va', 'ayer vi un perro'];

        getTweets.execute = jest.fn().mockReturnValue(mockTweets);
        await getTweetsController.execute(request, response);

        expect(getTweets.execute).toBeCalledWith(request.params.author);
        expect(response.status).toBeCalledWith(200);
        expect(response.send).toBeCalledWith(mockTweets);
    });

    test("Cant get tweets of an user", async () => {     
        getTweets.execute = jest.fn().mockRejectedValue(() => {throw new Error('Error')});
        await getTweetsController.execute(request, response);

        expect(getTweets.execute).toBeCalledWith(request.params.author);
        expect(response.status).toBeCalledWith(500);
        expect(response.send).toBeCalled();
    });
});
