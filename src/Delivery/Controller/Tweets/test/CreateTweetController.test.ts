import { CreateTweet } from '../../../../core/Actions/Tweets/CreateTweet';
import { CreateTweetController } from '../CreateTweetController';
import * as express from 'express';


describe("RegisterUserController use case", () => {
    const createTweet = {} as CreateTweet;
    const createTweetController = new CreateTweetController(createTweet);
    const request = { 
        body: {
            content: 'hola y chau'
        } 
    } as express.Request;
    request.params = { author: '@maga' };

    const response = {} as express.Response;
    response.status = jest.fn().mockReturnValue(response);
    response.send = jest.fn();

    beforeEach(() => {
        jest.clearAllMocks();
    })

    test("Can create a tweet", async () => {
        createTweet.execute = jest.fn();

        await createTweetController.execute(request, response);
        expect(createTweet.execute).toBeCalledWith(request.body.content, request.params.author);
        expect(response.status).toBeCalledWith(200);
        expect(response.send).toBeCalledWith('Tweet creado');
    });

    test("Cant create a tweet", async () => {
        createTweet.execute = jest.fn().mockRejectedValue(() => {throw new Error('Error')});
        
        await createTweetController.execute(request, response);
        expect(createTweet.execute).toBeCalledWith(request.body.content, request.params.author);
        expect(response.status).toBeCalledWith(500);
        expect(response.send).toBeCalled();
    });
});
