import { createTweetProvider, getTweetsProvider} from "../../../core/Actions/Tweets/TweetActionsProvider";
import { CreateTweetController } from "./CreateTweetController";
import { GetTweetsController } from "./GetTweetsController";



export function createTweetController(): CreateTweetController {
    const createTweet = createTweetProvider();
    return new CreateTweetController(createTweet);
}

export function getTweetsController(): GetTweetsController {
    const getTweets = getTweetsProvider();
    return new GetTweetsController(getTweets);
}

