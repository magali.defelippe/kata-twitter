import * as express from 'express';
import { GetTweets } from '../../../core/Actions/Tweets/GetTweets';


export class GetTweetsController {
    constructor(private action: GetTweets){}
    
    async execute(req: express.Request, res: express.Response): Promise<string| any> {
        try {
            const tweets = await this.action.execute(req.params.author);
            return res.status(200).send(tweets);
        } catch (error) {
            res.status(500).send(error.message);
        }
    }
}