import express, { Request, Response } from "express";

import {createTweetController, getTweetsController } from "../Controller/Tweets/TweetsControllerProvider";

export const tweetRouter = express.Router();


tweetRouter.post("/:author", async (req: Request, res: Response) => {
  const createController = createTweetController();
  await createController.execute(req,res);
});


tweetRouter.get("/:author", async (req: Request, res: Response) => {
  const getController = getTweetsController();
  await getController.execute(req,res);
});

