import express, { Request, Response } from "express";

import { newFollowController, getFollowersController, getFollowingsController} from "../Controller/Follows/FollowControllersProvider";

export const followRouter = express.Router();


followRouter.post("/", async (req: Request, res: Response) => {
  const followController = newFollowController();
  await followController.execute(req,res);
});

followRouter.get("/followers/:nickname", async (req: Request, res: Response) => {
    const followersController = getFollowersController();
    await followersController.execute(req,res);
});

followRouter.get("/followings/:nickname", async (req: Request, res: Response) => {
  const followingsController = getFollowingsController();
  await followingsController.execute(req,res);
});