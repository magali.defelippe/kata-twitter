import express, { Request, Response } from "express";

import { registerController, findController, changeNameController} from "../Controller/Users/UserControllersProvider";

export const userRouter = express.Router();


userRouter.post("/", async (req: Request, res: Response) => {
  const userRegisterController = registerController();
  await userRegisterController.execute(req,res);
});


userRouter.get("/:nickname", async (req: Request, res: Response) => {
  const userFindController = findController();
  await userFindController.execute(req,res);
});

userRouter.put("/:nickname", async (req: Request, res: Response) => {
  const changeNameUserController = changeNameController();
  await changeNameUserController.execute(req, res);
});